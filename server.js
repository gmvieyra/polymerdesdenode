var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var cors = require('cors');
app.use(cors());
app.listen(port);
app.use(express.static(__dirname + '/build/default'));
console.log('Ejecutando Polymer desde Node on: ' + port);

app.get('/', function(req, res) {
  res.sendFile('index.html',{root: '.'})
})
